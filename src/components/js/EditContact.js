import axios from 'axios'
import $ from 'jquery'

export default {
    name: 'AddPage',
    data () {
      return {
        msg: 'This is Add Contact Page',
        Data:[
          {
          contactID: '',
          firstName: '',
          lastName: '',
          gender: '',
          email: '',
          mobile: '',
          facebook:'',
          imageURL:'',
          }
        ],
      }
    },
    methods: {
      async editContact(contactID){
        console.log('enter editcontact')
        let newData = {
          contactID: this.Data[0].contactID,
          firstName: this.Data[0].firstName,
          lastName: this.Data[0].lastName,
          gender: this.Data[0].gender,
          email: this.Data[0].email,
          mobile: this.Data[0].mobile,
          facebook:this.Data[0].facebook,
          imageURL:this.Data[0].imageURL,
        }
        // console.log(newData)
        await axios.put('http://localhost:3000/contacts/update/' + contactID, this.Data[0])
        .then((res) =>{
          this.Data = res.data
          console.log(this.Data)
        })
        window.location.href='http://localhost:8081/#/Home'
      },
      setValue(){
        document.getElementById("contactid").value = this.Data[0].contactID;
        document.getElementById("firstname").value = this.Data[0].firstName;
        document.getElementById("lastname").value = this.Data[0].lastName;
        document.getElementById("gender").value = this.Data[0].gender;
        document.getElementById("mobile").value = this.Data[0].mobile;
        document.getElementById("email").value = this.Data[0].email;
        document.getElementById("facebook").value = this.Data[0].facebook;
        document.getElementById("imageURL").value = this.Data[0].imageURL;

       },
   },
  
   async beforeMount() {
    await axios.get('http://localhost:3000/contacts/searchByID/' + this.$route.query.contactID)
    .then((res) => {
      this.Data = res.data
      console.log(res.data)
    })
    .catch((error) => {
      console.log(error)
    })
    this.setValue()
    // window.location.reload() 
   }
}